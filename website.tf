# Note: The bucket name needs to carry the same name as the domain!
# http://stackoverflow.com/a/5048129/2966951
resource "aws_s3_bucket" "site" {
  bucket = "${var.domain}"
  acl = "public-read"

  policy = <<EOF
    {
        "Version":"2012-10-17",
        "Statement":[{
            "Sid":"PublicReadGetObject",
                "Effect":"Allow",
            "Principal": "*",
            "Action":["s3:GetObject"],
            "Resource":["arn:aws:s3:::${var.domain}/*"
            ]
            }
        ]
    }
  EOF

  website {
      index_document = "index.html"
  }
}

resource "aws_s3_bucket" "log_bucket" {
  bucket        = "my-general-logs"
  acl           = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}


# Note: Creating this route53 zone is not enough. The domain's name servers need to point to the NS
# servers of the route53 zone. Otherwise the DNS lookup will fail.
# To verify that the dns lookup succeeds: `dig site @nameserver`
resource "aws_route53_zone" "main" {
  name = "${var.domain}"
}

resource "aws_route53_record" "root_domain" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name = "${var.domain}"
  type = "A"

  alias {
    name = "${aws_cloudfront_distribution.cdn.domain_name}"
    zone_id = "${aws_cloudfront_distribution.cdn.hosted_zone_id}"
    evaluate_target_health = true
  }
}
resource "aws_route53_record" "ip6_root_domain" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name = "www.${var.domain}"
  type     = "AAAA"

  alias {
    name      = "${aws_cloudfront_distribution.cdn.domain_name}"
    zone_id   = "${aws_cloudfront_distribution.cdn.hosted_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name = "www.${var.domain}"
  type     = "A"

  alias {
    name                   = "${aws_route53_record.root_domain.name}"
    zone_id                = "${aws_route53_record.root_domain.zone_id}"
    evaluate_target_health = true
  }
}
resource "aws_route53_record" "google-webmaster" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  name = "www.${var.domain}"
  type     = "TXT"
  ttl      = "300"
  records  = ["google-site-verification=SS5LrrzgstC13zgb5bR1ZWd4pI534No7MaJboo-smNo"]
}

resource "aws_cloudfront_distribution" "cdn" {
  origin {
    origin_id   = "${var.domain}"
    domain_name = "${var.domain}.s3.amazonaws.com"
  }

  # If using route53 aliases for DNS we need to declare it here too, otherwise we'll get 403s.
  aliases = ["${var.domain}"]

  enabled             = true
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${var.domain}"

    forwarded_values {
      query_string = true
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # The cheapest priceclass
  price_class = "PriceClass_All"

  # This is required to be specified even if it's not used.
  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }
  logging_config {
    include_cookies = false
    bucket          = "${aws_s3_bucket.log_bucket.bucket_domain_name}"
    prefix          = "${var.domain}"
  }
  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

 output "website" {
  value = {
    "route53.domain"        = "${aws_route53_record.root_domain.fqdn}"
    "cdn.domain"            = "${aws_cloudfront_distribution.cdn.domain_name}"
    # "site.website_domain" = "${aws_s3_bucket.site.website_domain}"
  }
}