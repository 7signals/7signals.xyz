variable "aws_region" {
  default = "eu-west-1"
}

variable "domain" {
  default = "7signals.xyz"
}

provider "aws" {
  region = "${var.aws_region}"
  profile = "terraform"
}